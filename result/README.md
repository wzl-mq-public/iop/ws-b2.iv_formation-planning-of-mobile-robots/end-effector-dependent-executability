Extending the executability of assembly task poses by robots through end-effectors

Aline Kluge-Wilkes, Presley Demuner Reverdito

The following data set was created during the validation of a proposed method to evaluate assembly station formations considering the executability of assembly tasks, including the effects of equipped end-effectors on robots.

The underlying paper can be found at: https://doi.org/10.1007/978-3-031-34821-1_58 

The underlying source code can be found at: https://git-ce.rwth-aachen.de/wzl-mq-public/iop/ws-b2.iv_formation-planning-of-mobile-robots/end-effector-dependent-executability

In dependence on the geometries and degrees of freedom of the equipped end-effectors on the robot, a single task pose is transferred into an area of feasible task poses. Determining the executability of the task, the resulting representation of the feasible task poses is overlapped with the robot's workspace. If an overlap occurs, it can be assumed that there is a feasible robot configuration to execute the allocated assembly task. The proposed method is exemplified on a UR10 equipped with a screwdriver, and distributed task poses. The proposed method quantifies the end-effector's effect on the executability of assembly tasks and provides a means of determining the feasible base placement of robots in changeable assembly stations. Therefore, the method lays the foundation for automated formation planning in assembly stations.

Design of Experiments:

The published data here results from a conducted series of experiments structured as a full-factorial design of experiments. The following variables and expressions / data points of those variables were chosen:

Tasks ("GoalPose") poses as [x, y, z, x-quaternion, y-quaternion, z-quaternion, w-quaternion]:

    Task 1 = (0.416m ,-0.399m, 0.764m, 0.071, 0.703, 0.134, 0.694)
    Task 2 = (0.438m, -0.647m, 0.816m, 0.005, 0.707, 0.068, 0.704)
    Task 3 = (0.448m, -0.755m, 0.945m, -0.243, 0.664 ,-0.183, 0.683)

Dimension of the tools ("DimensionOfTool") as [x, y, z]: 

 (0.1m, 0.2m, 0m)
 (0.2m, 0.1m, 0m)

Robot model ("RobotModel"): 

 UR10 (https://www.universal-robots.com/de/produkte/ur10-roboter/)
 UR5 (https://www.universal-robots.com/products/ur5-robot/)

Resolution of reachability map ("ReachMap"): 

 0.05m
 0.08m
 0.1m

IK solver ("IKSolver"): 

TRAC-IK  

documented in: http://docs.ros.org/en/kinetic/api/moveit_tutorials/html/doc/trac_ik/trac_ik_tutorial.html 
source: https://bitbucket.org/traclabs/trac_ik/src/master/
KDL solver 

https://docs.orocos.org/kdl/overview.html

Based on these definitions, a fully factorial design was implemented, resulting in a total of 72 required tests (2³ × 3²). These tests were randomly organized into a single experimental block.

Test execution:

The function "[calcForAllTasks](https://git-ce.rwth-aachen.de/wzl-mq-public/iop/ws-b2.iv_formation-planning-of-mobile-robots/end-effector-dependent-executability/-/blob/main/code/main.py?ref_type=heads)" on the file main.py was created to generate 72 files, each containing 10 poses of the circle of possible poses for the robot with a calculated reachability map. These poses were manually tested using MoveIt! (https://moveit.ros.org/) and using two different IK solvers, KDL and TRAC-IK.

There were now four packages: ur5_kdl, ur5_trac_ik, ur10_kdl, and ur10_trac_ik. Each package had its own group_name, which was the same as the package name. For each test, it was necessary to run the MoveIt! file of the robot with the correspondent IK solver, and the [move_group_python_interface.py](https://git-ce.rwth-aachen.de/wzl-mq-public/iop/ws-b2.iv_formation-planning-of-mobile-robots/end-effector-dependent-executability/-/blob/main/code/move_group_python_interface.py?ref_type=heads) with the desired pose. On the Python code, the goal pose was set, and using the MoveGroupPythonIntefaceTutorial class, Moveit! attempt to move the robot to the desired pose. If the pose is reachable, the reachability index was set to 1. Otherwise, it was set to 0, and the terminal output would show "ABORTED: No motion plan found. No execution attempted."

After simulating all poses, the reachability average of each test was calculated, along with the sum of reachable results. The reachability index ranged from 0 to 100, and the reachable column ranged from 0 to 10. This result can be found in the file DoE-tests-and-results.xlsx.



The file "DoE-tests-and-results.xlsx" provides an overview of all experiments in its first table. The creation order ("StdOrder") and the order in which the experiments were conducted are given ("RunOrder"). The following columns in the first table indicate the expressions of the variables as explained above (GoalPose, DimensionOfTool, RobotMode, ReachMap, IKSolver). Next, the results of the experiments are given: the average reachability index per tested goal pose and the indication of how many of the ten discrete robot flange poses per goal pose are reachable by the robot (Reachability Index, Reachable). 

The following 72 tables each provide the ten robot flange poses per goal pose per experiment as [x, y, z, x-quaternion, y-quaternion, z-quaternion, w-quaternion]. 

The 72 .csv files contain the same information as the 72 tables in the Excel file but contain the information as it was transferred during execution. 





After all simulations, it is possible to conclude that the reachability depends on the position of the task and the model of the robot. For example, UR5 could not reach task 3, while the UR10 had a high value of reachability for it due to the fact that the UR10 has a bigger workspace.

-------------------------------------------------------------------------------------------------------------------------------------------------------

Acknowledgement:

Funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) under Germany's Excellence Strategy - EXC-2023 Internet of Production - 390621612.