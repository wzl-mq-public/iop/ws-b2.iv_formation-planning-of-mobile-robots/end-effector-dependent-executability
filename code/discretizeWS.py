import pybullet as p
import numpy as np

def discretize_workspace(urdf_file, voxel_size):
    # Load the robot in PyBullet
    p.connect(p.DIRECT)  # Connect to the physics engine
    robot_id = p.loadURDF(urdf_file)  # Load the URDF file

    # Get the bounding box of the robot
    aabb_min, aabb_max = p.getAABB(robot_id)
    aabb_min = np.array(aabb_min)  # Convert to numpy array

    # Calculate the dimensions of the workspace
    workspace_min = np.floor(aabb_min / voxel_size) * voxel_size
    workspace_dims = np.ceil((aabb_max - workspace_min) / voxel_size).astype(int)

    # Get the positions of all voxels in the workspace
    voxel_positions = []
    for x in range(workspace_dims[0]):
        for y in range(workspace_dims[1]):
            for z in range(workspace_dims[2]):
                voxel_pos = workspace_min + np.array([x, y, z]) * voxel_size
                voxel_positions.append(voxel_pos)

    return voxel_positions

# Example usage
urdf_file = "/home/ros1/spp-maa/src/universal_robot/ur_description/urdf/ur5.urdf"
voxel_size = 0.1  # Adjust the size of each voxel as needed

workspace_positions = discretize_workspace(urdf_file, voxel_size)
print("Number of positions in the discrete workspace:", len(workspace_positions))
print("Workspace positions:", workspace_positions)
