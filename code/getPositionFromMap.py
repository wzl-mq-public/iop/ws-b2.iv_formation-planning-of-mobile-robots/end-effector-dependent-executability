import h5py
import os
import random
import pandas as pd
from main import calcForAllTasks
from main import getReachabilityScore
import numpy as np


robot = 'ur10'
resolution = '0.60'
map = robot + '_' + resolution + '.h5'
file = 'result-'+robot+'_'+resolution+'.csv'
def addRandom(value):
    newvalue = float(value + random.uniform(-0.005,0.005))
    return newvalue

def getPosition(file):        
    file_dir = os.path.abspath(os.path.join(os.pardir,"/home/ros1/spp-maa/maps",file))
    f = h5py.File(file_dir,'r')
    spheres = f['Spheres']
    spheres_data = spheres['sphere_dataset']     
    return spheres_data

def hdf5toCSV(dataset):

    newdataframe = pd.DataFrame({"x": dataset[:,0],"y": dataset[:,1],"z":dataset[:,2], "index": dataset[:,3]})
    newdataframe.x = newdataframe.x.apply(addRandom)
    newdataframe.y = newdataframe.y.apply(addRandom)
    newdataframe.z = newdataframe.z.apply(addRandom)
    newdataframe.to_csv("PositionFromMap_" + str(robot) +'_'+ str(resolution) + '.h5'+".csv",index=False) 




hdf5toCSV(getPosition('/home/ros1/spp-maa/maps/' + str(robot) +'_'+ str(resolution) + '.h5'))
cols=['x','y','z','qx','qy','qz','qw','reachability index']
experiment = pd.read_csv("PositionFromMap_" + str(robot) +'_'+ str(resolution) + '.h5'+".csv", sep=',')
dataframe= pd.DataFrame(columns=cols)
score = []




for i in range(len(experiment)):
    
    x = float(experiment.iloc[i,0])
    y = float(experiment.iloc[i,1])
    z = float(experiment.iloc[i,2])

    goalpose = [x, y, z, 0, 0, 0, 1]


    result = calcForAllTasks(map,np.array(np.array([0,0,0,0,0,0,1])),np.array(np.array([goalpose])),
                    0.1,0.2,1,False,file,float(resolution)/2,False)
    
    dataframe = dataframe.append(pd.DataFrame([result[0]],columns=cols), 
     ignore_index=True)
    
dataframe.to_csv('result-'+robot+'_'+resolution+'.csv', index=False)
