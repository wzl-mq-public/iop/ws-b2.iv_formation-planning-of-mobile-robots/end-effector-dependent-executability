# This code read a DoE csv file generated from software Minitab, 
# process it to get the values and call the function calcForAllTasks 
import pandas as pd
import numpy as np
import sys
from main import calcForAllTasks
cols = ['StdOrder','RunOrder','Blocks','GoalPose','DimensionOfTool','RobotMode',
        'ReachMap','IKSolver']
experiment = pd.read_csv('/home/ros1/spp-maa/code/DoE.csv', usecols=cols, sep=',')


for i in range(len(experiment)):
    StdOrder = experiment.iloc[i, 0]
    RunOrder = experiment.iloc[i, 1]
    Blocks = experiment.iloc[i, 2]
    GoalPose = experiment.iloc[i, 3]
    DimensionOfTool = experiment.iloc[i, 4]
    RobotMode = experiment.iloc[i, 5]
    ReachMap = experiment.iloc[i, 6]
    

    
    
    goalpose_array = np.fromstring(GoalPose, dtype=float, sep=',',count = 7)
    dimension = np.fromstring(DimensionOfTool, dtype=float, sep=',',count = 2)
    
    

    if RobotMode == "UR10":
        calcForAllTasks('ur10_'+str(ReachMap)+'.h5', np.array([0,0,0,0,0,0,1]),np.array([goalpose_array]),dimension[0],dimension[1],10,
                        True,'result-'+'RunOrder-'+str(RunOrder)+'.csv',ReachMap/2,False)
        
        

    else:
        calcForAllTasks('ur5_'+str(ReachMap)+'.h5', np.array([0,0,0,0,0,0,1]),np.array([goalpose_array]),dimension[0],dimension[1],10,
                        True,'result-'+'RunOrder-'+str(RunOrder)+'.csv',ReachMap/2,False)
    
        
    print("Experiment (" + str(i+1 ) + " / " + str(len(experiment)) + ") is done!")
    sys.stdout.write("\033[F")
    