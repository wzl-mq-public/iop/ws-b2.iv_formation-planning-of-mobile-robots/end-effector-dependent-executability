# This code read a csv file of all poses of a robot workspace
# send each pose to move_group_python_interface_tutorial.py. For each sent pose
# there is a question if the pose was reachable
import pandas as pd

# Information about the robot and the reach. map
robot="ur10"
resolution="0.60"

# File with all workspace poses
csvfile = "PositionFromMap_" + str(robot) +'_'+ str(resolution) + '.h5'+".csv"


resultFile = "result-" + str(robot) +'_'+ str(resolution) +".csv"
sheet = pd.read_csv(resultFile)
reacheable = pd.DataFrame(columns=['reachable'])

# If you don't wanna start from the beginning of the file, insert the number of the line
start = 0
start = int(input("Where do you want to start (starting with 0)"))

for i in range(start, len(sheet)):
    with open('/home/ros1/spp-maa/src/moveit_tutorials/doc/move_group_python_interface/scripts/move_group_python_interface_tutorial.py', 'r') as file:
        # read a list of lines into data
        code = file.readlines()

    code[62] = "pose_goal.position.x = " + str(sheet.x[i]) + "\n"
    code[63] = "pose_goal.position.y = " + str(sheet.y[i])+ "\n"
    code[64] = "pose_goal.position.z = " + str(sheet.z[i])+ "\n"

    with open('/home/ros1/spp-maa/src/moveit_tutorials/doc/move_group_python_interface/scripts/move_group_python_interface_tutorial.py', 'w') as file:
        file.writelines( code )

    # Save the result on the dataframe and aftwards send it to result csv file
    try:
        reacheable = reacheable.append(pd.DataFrame([int(input("Is position " + str(i) + " reachable? (1 or 0)"))],columns=['reachable']), 
     ignore_index=True) 
        new_df = pd.concat([sheet,reacheable], axis=1)
        new_df.to_csv(csvfile, index=False)

        input("Press enter...")
    except SyntaxError:
        pass

    